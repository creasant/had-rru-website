$(document).ready(function(){

    var prev_scrollY = 0;

    function update_top(){
        var t_h = $('header').outerHeight();
        $('body').css('padding-top', t_h);
        $('.mobile_header-nav').css('padding-top', t_h);
        // document.documentElement.style.setProperty('--headerHeight', $('header').outerHeight()+'px');
        if($('header').hasClass('scroll_top')){
            $('header').css('top', 0-$('header').height());
        } else {
            $('header').css('top', 0);
        }
    };

    function update_menu(){
        if($('body').hasClass('menu-active')){
            $('.menu-trigger').find('.hamburger').addClass('is-active');
            $('.mobile_header-nav_wrapper').stop().slideDown();
        } else {
            $('.menu-trigger').find('.hamburger').removeClass('is-active');
            $('.mobile_header-nav_wrapper').stop().slideUp();
        }
    };

    function update_region_card(){
        $('.region-card').each(function (idx) {
            $('.region-card').css('font-size', $('.region-card').width());
        });
    };

    $(".open-hotel-search-advance").on("click", function (evt) {

        if($(".filter-hotel-advance").css('display') == 'none'){
            $(".filter-hotel-advance").show();
        } else {
            $(".filter-hotel-advance").hide();
        }
    });

    $(".btn-print").on("click", function (evt) {
        evt.preventDefault();
        window.print();
    });

    $(".menu-trigger").on('click', function(){
        var t_wrapper = $('body');
        t_wrapper.toggleClass('menu-active');
        update_menu();
        update_top();
    });

    $(".top-highlight_close").on('click', function(){
        $('.top-highlight').stop().slideUp(400);
        for (let index = 0; index < 4; index++) {
            for (let index2 = 1; index2 <= 5; index2++) {
                setTimeout(() => {
                    update_top();
                }, (index*100)+(index2*20+1));
            }
        }
    });


    // $("#article_search_more, .article_search_lightbox_overlay, .article_search_lightbox_close").on('click', function(){
    //     $('.article_search_lightbox_wrapper').toggleClass('active');
    // });

    $(".content-group.accordion .content-group-title").on('click', function(){
        var t_wrapper = $(this).closest('.content-group');
        t_wrapper.toggleClass('active');
        if(t_wrapper.hasClass('active')){
            t_wrapper.find('.content-group-text').stop().slideDown();
        } else {
            t_wrapper.find('.content-group-text').stop().slideUp();
        }
    });

    $(".content-accordion-block .content-accordion-block-title").on('click', function(){
        var t_wrapper = $(this).closest('.content-accordion-block');
        t_wrapper.toggleClass('active');
        if(t_wrapper.hasClass('active')){
            t_wrapper.find('.content-accordion-block-text').stop().slideDown();
        } else {
            t_wrapper.find('.content-accordion-block-text').stop().slideUp();
        }
    });

    $(".mobile_header-nav-link").on('click', function(e){
        if($(this).closest('.mobile_header-nav_item.has-sub.open-sub').hasClass('open-sub')){
            if(!$(this).is('[href]')){
                e.preventDefault();
                $('.mobile_header-nav_item.has-sub').removeClass('open-sub');
            }
        } else {
            e.preventDefault();
            $('.mobile_header-nav_item.has-sub').removeClass('open-sub');
            $(this).closest('.mobile_header-nav_item.has-sub').addClass('open-sub');
        }
    });

    $(".page-tab_curr, .page-tab_trigger").on('click', function(){
        $('.page-tab_list_wrapper').toggleClass('open-tab');
    });

    $(".scroll-top").on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    // function update_tab(item){
    //     var tabid = $(item).data('id');
    //     var tabtitle = $(item).html();
    //     $('.page-tab_list_item').removeClass('active');
    //     $(item).addClass('active');

    //     $('.page-tab_list_wrapper').removeClass('open-tab');
    //     $('.page-tab').css('display', 'none');
    //     $('.page-tab[data-id="'+tabid+'"]').css('display', 'block');
    //     $('.page-tab_curr').html(tabtitle);
    //     $('.breadcrumb-item:last').html(tabtitle);
    // }

    // $(".page-tab_list_item").on('click', function(){
    //     update_tab(this);
    // });

    $(".content-section.content-slider").each(function(idx){
        var temp_id = 'obj-'+idx;
        $(this).addClass(temp_id);
        new Swiper('.content-section.content-slider.'+temp_id+' .swiper-container', {
            slidesPerGroup: 4,
            slidesPerView: 4,
            spaceBetween: 35,
            breakpoints: {
                520: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                },
                760: {
                    slidesPerGroup: 2,
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                960: {
                    slidesPerGroup: 3,
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1350: {
                    slidesPerGroup: 4,
                    slidesPerView: 4,
                    spaceBetween: 30,
                }
            },
            navigation: {
                nextEl: '.content-section.content-slider.'+temp_id+' .swiper-button-next',
                prevEl: '.content-section.content-slider.'+temp_id+' .swiper-button-prev',
            },
            pagination: {
                clickable: true,
                el: '.content-section.content-slider.'+temp_id+' .swiper-pagination',
            },
        });
    });

    $(window).resize(function(){
        var w_w = window.innerWidth;
        if(w_w > 960){
            $('body').removeClass('menu-active');
            $('.mobile_header-nav_wrapper').removeAttr('style');
            $('.hamburger').removeClass('is-active');

            $('.page-tab_list_wrapper').removeClass('open-tab');
        }

        update_top();

        if($('.article_slider').length){
            // $('.article_slider .swiper-button-next').css('top', $('.display-desktop .section-article-icon').eq(0).height()/2 + parseInt($('.article_slider').css('paddingTop')));
            // $('.article_slider .swiper-button-prev').css('top', $('.display-desktop .section-article-icon').eq(0).height()/2 + parseInt($('.article_slider').css('paddingTop')));
        }
    });

    $(window).on("orientationchange",function(){
        $(window).resize();
    });

    $(window).scroll(function(){
        if(window.scrollY > $('header').outerHeight()){
            $('header').addClass('fixed');
            if(prev_scrollY < window.scrollY){
                $('header').addClass('scroll_top');
            } else {
                $('header').removeClass('scroll_top');
            }
        } else {
            $('header').removeClass('fixed');
            $('header').removeClass('scroll_top');
        }
        prev_scrollY = window.scrollY;
        update_top();
        update_region_card();
    });



    $(window).resize();
    // if($('.page-tab_list .page-tab_list_item').length){
    //     update_tab($('.page-tab_list .page-tab_list_item').eq(0));
    // }

});
