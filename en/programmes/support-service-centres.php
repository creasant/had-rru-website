<?php



$title = "Race Relations Unit - Programmes and Services "; // Web Title on Tab

$group_title = "Programmes and Services"; // Control Breadcrumb Title
$page_title = "Support Service Centres for Ethnic Minorities"; // Control Which tab is active and set the page title
$revisionDate = '19 March 2021'; // Last revision date on Footer
$current_path = pathinfo(__FILE__, PATHINFO_BASENAME);

include_once '../../include/config.inc.php'; // Include $root_path setting

include_once './page_header.inc.php'; //Include page-header , breadcrumb





include_once '../templates/header.php'; // Include Header
// include_once './page_header.inc.php'; //Include page-header , breadcrumb
// include_once './page_tab_list_wrapper.inc.php'; //Include the tab

?>

<div class="page-content">
    <div class="site-wrapper">
        <h2>Support Service Centres for Ethnic Minorities</h2>
        <div class="content-text content-style">
            <h3 class="subtitle">Introduction</h3>
            <p>To facilitate the integration of ethnic minorities into the community, and enhance their access to public services, Government is funding non-profit-making organisations (NPOs) to operate six support service centres and two sub-centres for ethnic minorities.</p>

            <h3 class="subtitle">Operators and Addreses</h3>
            <div class="table-legend">
                <span class="legend">
                    <img src="<?php echo $BASE_URL?>images/icon-map--dark.svg" alt="">Location Map
                </span>
                <span class="legend">
                    <img src="<?php echo $BASE_URL?>images/icon-marker--dark.svg" alt="">Link to Geolnfo Map
                </span>
            </div>
            <div class="reponsive-shadow-table-wrapper">
                <div class="reponsive-shadow-table">
                    <table class="content-table">
                        <thead>
                            <tr>
                                <th class="address">
                                    Operators / Addresses
                                </th>
                                <th class="enquiry">
                                    Enquiry
                                </th>
                                <th class="leaflet">
                                    Leaflet
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Hong Kong Christian Service <br/>
                                                        <a href="#">CHEER Centre</a><br/>
                                                        4/F, 64 Tsun Yip Street<br/>
                                                        South Asia Commercial Centre<br/>
                                                        Kwun Tong, Kowloon<br/>
                                                        Email address: <a href="mailto:cheer@hkcs.org">cheer@hkcs.org</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3106 3104</td>
                                                <td class="leaflet" rowspan="10"><img src="<?php echo $BASE_URL?>images/leaflet-01.jpg" alt=""></td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Free Telephone Interpretation Service
                                                    </div>
                                                </td>
                                                <td class="enquiry">3106 3104</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Bahasa Indonesia
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6822</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Nepali
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6833</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Urdu
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6844</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Punjabi
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6855</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Tagalog
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6866</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Thai
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6877</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Hindi
                                                    </div>
                                                </td>
                                                <td class="enquiry">3755 6888</td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Vietnamese
                                                    </div>
                                                </td>
                                                <td class="enquiry"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        New Home Association <br/>
                                                        <a href="#">HOME Centre (Yau Tsim Mong Centre)</a><br/>
                                                        Shop B, G/F & 1/F, Sun Wah Building<br/>
                                                        73 Battery Street<br/>
                                                        Yau Ma Tei, Kowloon<br/>
                                                        Email address:<br/>
                                                        <a href="mailto:homeytm@nha.org.hk">homeytm@nha.org.hk</a> / <a href="mailto:homeytm@nhahome.hk">homeytm@nhahome.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3610 4418</td>
                                                <td class="leaflet" rowspan="2"><img src="http://project24.creasant.in/gov_rru//images/leaflet-02.jpg" alt=""></td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        <a href="#">HOME Centre (Yau Tsim Mong Centre)</a><br/>
                                                        Shop A, G/F, South Ocean Building<br/>
                                                        130 Kiu Kiang Street<br/>
                                                        Sham Shui Po, Kowloon<br/>
                                                        Email address:<br/>
                                                        <a href="mailto:homeytm@nha.org.hk">homeytm@nha.org.hk</a> / <a href="mailto:homeytm@nhahome.hk">homeytm@nhahome.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3610 4428</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        International Social Service - Hong Kong Branch<br/>
                                                        <a href="#">HOPE Centre</a><br/>
                                                        3/F Tak Lee Commercial Building,<br/>
                                                        113-117 Wanchai Road,<br/>
                                                        Wan Chai, Hong Kong<br/>
                                                        Email address:
                                                        <a href="mailto:ethnic_centre@isshk.org">ethnic_centre@isshk.org</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">5188 8044<br/><br/>2836 3598</td>
                                                <td class="leaflet" rowspan="2"><img src="<?php echo $BASE_URL?>images/leaflet-03.jpg" alt=""></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Hong Kong Community Network<br/>
                                                        <a href="#">LINK Centre</a><br/>
                                                        Shop B-E, G/F, Cheong Nin Building,<br/>
                                                        1013-1033 Kwai Chung Road, Kwai Chung, N.T.<br/>
                                                        Email address:
                                                        <a href="mailto:link@hkcn.org.hk">link@hkcn.org.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3955 1555</td>
                                                <td class="leaflet" rowspan="2"><img src="<?php echo $BASE_URL?>images/leaflet-04.jpg" alt=""></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Christian Action <br/>
                                                        <a href="#">SHINE Centre</a><br/>
                                                        Shop B2<br/>
                                                        Tuen Mun Central Square<br/>
                                                        22 Hoi Wing Road<br/>
                                                        Tuen Mun, N.T.<br/>
                                                        Hong Kong<br/>
                                                        Email address:
                                                        <a href="mailto:shine@christian-action.org.hk">shine@christian-action.org.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3188 4555</td>
                                                <td class="leaflet" rowspan="2"><img src="http://project24.creasant.in/gov_rru//images/leaflet-05.jpg" alt=""></td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        <a href="#">SHINE Centre Community Development Team</a><br/>
                                                        3/F & 4/F, Lee Kong Commercial Building<br/>
                                                        115 Woosung Street<br/>
                                                        Jordan, Kowloon<br/>
                                                        Email address:
                                                        <a href="mailto:nfo@sscem.org">nfo@sscem.org</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">2617 1369</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        The Neighbourhood Advice-Action Council<br/>
                                                        <a href="#">TOUCH Centre (Tung Chung Sub-centre)</a><br/>
                                                        Left Wing, 1/F, Tung Chung Community Services Complex<br/>
                                                        420 Tung Chung Road<br/>
                                                        Tung Chung, Lantau Island<br/>
                                                        Email address:
                                                        <a href="mailto:scem@naac.org.hk">scem@naac.org.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="<?php echo $BASE_URL?>images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">2988 1422</td>
                                                <td class="leaflet" rowspan="2"><img src="<?php echo $BASE_URL?>images/leaflet-06.jpg" alt=""></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="operator-wrapper">
                                        <thead>
                                            <th class="address"></th>
                                            <th class="enquiry"></th>
                                            <th class="leaflet"></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        <a href="#">Yuen Long Town Hall<br/>
                                                        Support Service Centre for Ethnic Minorities</a><br/>
                                                        3/F, Yuen Long District Community Services Building,<br/>
                                                        4 Yuen Long Tai Yuk Road,<br/>
                                                        Yuen Long, N.T.<br/>
                                                        Email address:
                                                        <a href="mailto:info@sscem.org">info@sscem.org</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">2479 9757</td>
                                                <td class="leaflet" rowspan="2"><img src="http://project24.creasant.in/gov_rru//images/leaflet-07.jpg" alt=""></td>
                                            </tr>
                                            <tr>
                                                <td class="address">
                                                    <div class="address-container">
                                                        Chomolongma Multicultural Community Centre<br/>
                                                        Wing Ning Village<br/>
                                                        Yung Yuen Road<br/>
                                                        Ping Shan, Yuen Long<br/>
                                                        New Territories<br/>
                                                        Email address:
                                                        <a href="mailto:nfo@sscem.org">emisc@christian-action.org.hk</a><br/>
                                                        <span class="quicklinks">
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-map.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-marker.svg" alt=""></a>
                                                            <a href="#"><img src="http://project24.creasant.in/gov_rru//images/icon-facebook.svg" alt=""></a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="enquiry">3422 3820</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>



            <h3 class="subtitle">Services</h3>
            <p>All six centres and two sub-centres provide various tailor-made learning classes, after-school tutorial classes, and dedicated programmes for ethnic minority youths, as well as counselling and referral services, integration programmes, etc., to help ethnic minorities integrate into the community.</p>
            <p>The centre in Kwun Tong, operated by the Hong Kong Christian Service, provides centralised telephone interpretation service to assist ethnic minorities in their use of public services.</p>
            <h3 class="subtitle">Enquiry</h3>
            <p>For enquiries, please call 2835 1747.</p>


        </div>

    </div>
</div>

<style>
    .table-legend{
        display: block;
        text-align: right;
        font-size: 15px;
        font-weight: 400;
        margin-top: -70px;
        margin-bottom: 35px;
    }

    .table-legend .legend{
        margin-left: 30px;
        line-height: 30px;
    }

    .table-legend .legend img{
        margin-right: 5px;
        height: 30px;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody tr{
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody tr td{
        border-right: 0;
    }

    .reponsive-shadow-table-wrapper table thead th.address{
        width: 40%;
        min-width: 600px;
    }
    .reponsive-shadow-table-wrapper table thead th.enquiry{
        width: 30%;
        min-width: 400px;
    }
    .reponsive-shadow-table-wrapper table thead th.leaftlet{
        width: auto;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper{
        width: 100%;
        margin: 40px 0;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper thead{
        visibility: hidden;
    }
    .reponsive-shadow-table-wrapper table.operator-wrapper thead tr th{
        padding: 0!important;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody tr td {
        background-color: transparent;
        border: 0;
        border-right: none;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody tr:nth-child(2n+1) td {
        background-color: transparent;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody .address{
        text-align: left;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody .address-container{
        padding-left: 30px;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody .enquiry{
    }

    .reponsive-shadow-table-wrapper table.operator-wrapper tbody .leaflet{
        vertical-align: middle;
    }

    .quicklinks{
        display: block;
    }

    .quicklinks a{
        display: inline-block;
        margin-top: 20px;
        margin-right: 10px;
        margin-bottom: 20px;
        min-width: 40px;
        text-align: center;
    }

    .quicklinks a img{
        height: 40px;
    }


    @media only screen and (max-width: 1400px) {
        .reponsive-shadow-table-wrapper.on-start:before{
            display: none;
        }

        .reponsive-shadow-table-wrapper.on-end:after{
            display: none;
        }

        .reponsive-shadow-table > table{
            min-width: 1400px;
        }
    }


    @media only screen and (max-width: 960px) {
        .reponsive-shadow-table > table{
            min-width: 960px;
        }
        .reponsive-shadow-table-wrapper table thead th.address{
            min-width: 500px;
        }
        .reponsive-shadow-table-wrapper table thead th.enquiry{
            min-width: 250px;
        }
        .reponsive-shadow-table-wrapper table thead th.leaftlet{
            width: auto;
        }
    }

    @media only screen and (max-width: 767px) {
        .table-legend{
            font-size: 12px;
            margin-top: 0;
            margin-bottom: 10px;
        }

        .table-legend .legend {
            margin-left: 15px;
        }

        .table-legend .legend img{
            height: 24px;
            line-height: 24px;
        }

        .reponsive-shadow-table > table{
            min-width: 767px;
        }

        .reponsive-shadow-table-wrapper table thead th.address{
            min-width: 320px;
        }
        .reponsive-shadow-table-wrapper table thead th.enquiry{
            min-width: 200px;
        }
        .reponsive-shadow-table-wrapper table thead th.leaftlet{
            width: auto;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper{
            margin: 30px 0;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .address-container{
            padding-left: 20px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .enquiry{
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .leaflet{
            padding-right: 20px;
        }

        .quicklinks a{
            min-width: 32px;
        }
        .quicklinks a img{
            height: 32px;
        }

    }

    @media only screen and (max-width: 576px) {
        .reponsive-shadow-table > table{
            min-width: 576px;
        }

        .reponsive-shadow-table-wrapper table thead th.address{
            min-width: 300px;
        }
        .reponsive-shadow-table-wrapper table thead th.enquiry{
            min-width: 150px;
        }
        .reponsive-shadow-table-wrapper table thead th.leaftlet{
            width: auto;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper{
            margin: 20px 0;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .address-container{
            padding-left: 10px;
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .enquiry{
        }

        .reponsive-shadow-table-wrapper table.operator-wrapper tbody .leaflet{
            padding-right: 10px;
        }


    }

</style>

<script>

    $(document).ready(function() {
        var scrollLeft = $(".reponsive-shadow-table").scrollLeft();
        var resizeTimer;
        var scrollTimer;
        var tableWidth = $(".reponsive-shadow-table table").width();
        var tableViewWidth = $(".reponsive-shadow-table").width();

        if(scrollLeft == 0){
            $(".reponsive-shadow-table-wrapper").addClass("on-start");
        }else{
            $(".reponsive-shadow-table-wrapper").removeClass("on-start");
        }

        if(scrollLeft >= tableWidth - tableViewWidth){
            $(".reponsive-shadow-table-wrapper").addClass("on-end");
        }else{
            $(".reponsive-shadow-table-wrapper").removeClass("on-end");
        }


        $(window).on('resize', function(e) {

            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                var scrollLeft = $(".reponsive-shadow-table").scrollLeft();

                if(scrollLeft == 0){
                    $(".reponsive-shadow-table-wrapper").addClass("on-start");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-start");
                }

                if(scrollLeft == tableWidth - tableViewWidth){
                    $(".reponsive-shadow-table-wrapper").addClass("on-end");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-end");
                }

            }, 250);

        });

        $(".reponsive-shadow-table").on('scroll', function(e) {

            clearTimeout(scrollTimer);
            scrollTimer = setTimeout(function() {
                var scrollLeft = $(".reponsive-shadow-table").scrollLeft();

                if(scrollLeft == 0){
                    $(".reponsive-shadow-table-wrapper").addClass("on-start");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-start");
                }

                tableWidth = $(".reponsive-shadow-table table").width();
                tableViewWidth = $(".reponsive-shadow-table").width();

                if(scrollLeft >= tableWidth - tableViewWidth){
                    $(".reponsive-shadow-table-wrapper").addClass("on-end");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-end");
                }

            }, 150);

        });
    });

</script>

<?php
include_once '../templates/footer.php';
?>