<?php



$title = "Race Relations Unit - Programmes and Services "; // Web Title on Tab

$group_title = "Programmes and Services"; // Control Breadcrumb Title
$page_title = "Harmony Scholarships Scheme"; // Control Which tab is active and set the page title
$revisionDate = '19 March 2021'; // Last revision date on Footer
$current_path = pathinfo(__FILE__, PATHINFO_BASENAME);

include_once '../../include/config.inc.php'; // Include $root_path setting

include_once './page_header.inc.php'; //Include page-header , breadcrumb





include_once '../templates/header.php'; // Include Header
// include_once './page_header.inc.php'; //Include page-header , breadcrumb
// include_once './page_tab_list_wrapper.inc.php'; //Include the tab

?>

<div class="page-content">
    <div class="site-wrapper">
        <h2>Harmony Scholarships Scheme</h2>
        <div class="content-text content-style">
            <h3 class="subtitle">Background</h3>
            <p>The Harmony Scholarships Scheme was first launched in the academic year 2003/04 to recognise students’ participation in school and community services (particularly activities promoting racial harmony), their academic results and conduct.</p>

            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2020/21</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2018/19</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2017/18</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2016/17</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2015/16</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2014/15</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2013/14</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2012/13</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2011/12</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2010/11</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2009/10</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2008/09</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2006/07</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>
            <div class="content-group accordion">
                <div class="content-group-title">Harmony Scholarships Scheme  2005/06</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to join the Harmony Scholarships Scheme 2020/21.</p>
                        <p>Scholarships valued at $1,000 and $2,000 each will be awarded respectively to primary and secondary school students nominated by their schools. Please refer to <a href="#">scheme details</a> for information.</p>
                        <p>Schools interested in joining the Scheme are requested to complete and submit the <a href="#">nomination form</a> to us by 15 January 2021.</p>
                        <p>For enquiries, please contact Miss Kelly HO at 2835 1463.</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<?php
include_once '../templates/footer.php';
?>