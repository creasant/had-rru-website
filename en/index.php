<?php





$title = "Race Relations Unit - home";

$revisionDate = '19 March 2021';
// $revisionDate = date('d F Y', strtotime('03/19/2021'));

$with_highhighlight  = 1;


include_once '../include/config.inc.php';
include_once './templates/header.php';
?>

<div class="site-outer">
    <div class="index_slider">
        <!-- <div class="swiper-container">
            <div class="swiper-wrapper"> -->
        <div class="swiper-slide bg-cover">
            <div class="display-desktop">
                <h1>Race Relations Unit</h1>
                <img alt=""  src="<?php echo $root_path; ?>images/home-banner.jpg">
            </div>
            <div class="display-mobile">
                <h1>Race Relations Unit</h1>
                <img alt=""  src="<?php echo $root_path; ?>images/home-banner-mob.jpg">
            </div>
        </div>
        <!-- </div> -->
        <!-- Add Pagination -->
        <!-- <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div> -->
        <!-- <div class="swiper-pagination"></div>
        </div> -->
    </div>
</div>

<div class="index-section">
    <div class="site-wrapper">
        <div class="index-section-title">What's New</div>
        <div class="content-section">
            <h3>Updated information on Coronavirus Disease 2019 (COVID-19) - Government continues to implement compulsory testing notices for several buildings in Yau Ma Te</h3>
            <div class="content-text content-style">
                <p>Recently, a number of confirmed COVID-19 cases were found in the district of Yau Ma Tei and Jordan over a short period of time. To cut the transmission chain, the Government has been issuing compulsory testing notices. Failing to comply with the compulsory testing notice is an offence and may be fined a fixed penalty of $5,000. The Government encourages residents in Yau Ma Tei district to undergo testing proactively and promptly, even if they are not subject to compulsory testing. Please visit the "<a href="#">COVID-19 Thematic Website</a>" for latest updates.</p>
            </div>
            <h3>Video - Appeal on Testing for COVID-19 in the District of Yau Ma Tei and Jordan</h3>
            <div class="video-section">
                <div class="video-item">
                    <div class="video-item-container">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Xmnl4Nz7pgY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <div class="video-item-lang">Nepali – नेपाली</div>
                </div>
                <div class="video-item">
                    <div class="video-item-container">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/t12aNpIoYYI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <div class="video-item-lang">Urdu – اردو<</div>
                </div>
            </div>
            <h3>Video - Appeal on Prevention of COVID-19</h3>
            <div class="video-section">
                <div class="video-item">
                    <div class="video-item-container">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/XudaQKXqQTY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <div class="video-item-lang">Nepali – नेपाली</div>
                </div>
                <div class="video-item">
                    <div class="video-item-container">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/nCj9BsGgKfk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <div class="video-item-lang">Urdu – اردو<</div>
                </div>
            </div>
        </div>

        <div class="content-section">
            <h3>Invitation for Nomination for Harmony Scholarships Scheme 2020/21</h3>
            <div class="content-text content-style">
                <p>The Home Affairs Department is inviting schools enrolled with 30 or more non-Chinese speaking students to nominate students for the Harmony Scholarships. Schools interested in joining the Harmony Scholarships Scheme 2020/21 are requested to submit  nomination forms by 15 January 2021. Please refer to the <a href="#">programme page</a> for details.</p>
            </div>
        </div>

        <div class="content-section">
            <h3>District-based Programmes for Racial Harmony 2020-21</h3>
            <div class="content-img">
                <div class="content-img_single_item">
                    <img alt="" src="<?php echo $root_path; ?>images/news-01.jpg">
                </div>
            </div>
            <div class="content-text-with-img content-style">
                <p>The Home Affairs Department has launched the District-based Programmes for Racial Harmony to sponsor non-governmental organisations (NGOs) to organise district-based activities toencourage interaction and exchange between the ethnic minority (EM) and local communities. Please refer to the <a href="#">project page</a> for proposal submission requirement and other details.</p>
            </div>
        </div>

        <div class="content-section">
            <h3>Prevention of Novel Coronavirus Infection</h3>
            <div class="content-img">
                <div class="content-img_single_item">
                    <img alt="" src="<?php echo $root_path; ?>images/news-02.jpg">
                </div>
            </div>
            <div class="content-text-with-img content-style">
                <p>To prevent the spread of novel coronavirus in the community, members of the public should maintain good personal hygiene at all times and keep both hands clean. For details, please refer to our COVID-19 information page or the Centre for <a href="#">Health Protection’s thematic webpage</a>.</p>
            </div>
        </div>

        <div class="content-section">
            <h3>Innovative Programme by Support Service Centres for Ethnic Minorities 2019-20</h3>
            <div class="content-img">
                <div class="content-img_single_item">
                    <img alt="" src="<?php echo $root_path; ?>images/news-03.jpg">
                </div>
            </div>
            <div class="content-text-with-img content-style">
                <p>The Home Affairs Department has funded the Support Service Centres for Ethnic Minorities (EMs) to organise innovative programmes to help EM new arrivals integrate into community and/or the personal growth and development of EM youths. Please refer to the <a href="#">project page</a> for details.</p>
            </div>
        </div>


    </div>
</div>

<div class="index-section section-grey">
    <div class="site-wrapper">
        <div class="index-section-title">Feature Articles</div>
        <div class="article_slider">
            <div class="display-desktop">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-article-item">
                                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display-mobile">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="section-useful-list">
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                    <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                                    <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                                    <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                    <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="section-useful-list">
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                    <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                                    <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                                    <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
                                </div>
                                <div class="section-article-item">
                                    <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                                    <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <div class="text-center"><a class="btn" id="article_search_more" href="javascript:;">Read More</a></div>
    </div>
</div>

<div class="article_search_lightbox_wrapper">
    <div class="article_search_lightbox_overlay"></div>
    <div class="article_search_lightbox">
        <div class="article_search_lightbox_close"></div>
        <div class="article_search_full_list">
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-01.jpg"></div>
                <div class="section-article-text">District-based Programme for Racial Harmony 2019-20</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-02.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Non-ethnic Chinese Youths (Chinese Only)</div>
            </div>
            <div class="article_search_full-item">
                <div class="section-article-icon"><img alt=""  src="<?php echo $root_path; ?>images/feature-article-03.jpg"></div>
                <div class="section-article-text">Lifesaving Training Incentive Programme for Ethnic Minority Youths</div>
            </div>
        </div>
    </div>
</div>

<!-- FCV POPUP :: [s] -->
<div id="fvc_popup" class="fcv_modal">
    <div class="fcv_modal-backdrop"></div>
    <div class="fcv_modal-dialog">
        <div class="fcv_modal-content">
            <div class="fcv_modal-header">
                <div class="fcv_modal-close"></div>
            </div>
            <div class="fcv--container">
                <div class="fcv--banner">
                    <img class="desktop" src="<?php echo $root_path; ?>images/fcv_EN.jpg" alt="">
                    <img class="mobile" src="<?php echo $root_path; ?>images/fcv_EN_m.jpg" alt="">
                </div>
                <div class="fcv--table">
                    <h3 class="fcv--table-header">Thematic Webpage of Coronavirus disease (COVID-19) (Centre for Health Protection, Department of Health)</h3>
                    <table class="content-table">
                        <tbody><tr class="content">
                            <td><a href="https://www.coronavirus.gov.hk/chi/index.html" target="_blank">中文</a></td>
                            <td><a href="https://www.coronavirus.gov.hk/eng/index.html" target="_blank">English</a> </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://www.chp.gov.hk/en/features/102790.html" target="_blank">Indonesia - Bahasa Indonesia</a></td>
                            <td><a href="https://www.chp.gov.hk/en/features/102743.html" target="_blank">India - हिन्दी</a> </td>
                            <td><a href="https://www.chp.gov.hk/en/features/102786.html" target="_blank">Nepal - नेपाली</a></td>
                        </tr>
                            <tr>
                            <td><a href="https://www.chp.gov.hk/en/features/102791.html" target="_blank">Philippines - Tagalog</a></td>
                            <td><a href="https://www.chp.gov.hk/en/features/102788.html" target="_blank">Thailand - ภาษาไทย</a></td>
                            <td><a href="https://www.chp.gov.hk/en/features/102787.html" target="_blank">Pakistan - اردو   </a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="fcv--table">
                    <h3 class="fcv--table-header">Prevent pneumonia and respiratory tract infection (Simplified version by Centre for Health Protection, Department of Health)</h3>
                    <table class="content-table">
                        <tbody><tr class="content">
                            <td><a href="https://www.chp.gov.hk/files/pdf/factsheetchi_wuhan.pdf" target="_blank">中文</a></td>
                            <td><a href="https://www.chp.gov.hk/files/pdf/factsheeteng_wuhan.pdf" target="_blank">English</a> </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_indonesian.pdf" target="_blank">Indonesia - Bahasa Indonesia</a></td>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_hindi.pdf" target="_blank">India - हिन्दी</a> </td>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_nepali.pdf" target="_blank">Nepal - नेपाली</a></td>
                        </tr>
                            <tr>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_tagalog.pdf" target="_blank">Philippines - Tagalog</a></td>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_thai.pdf" target="_blank">Thailand - ภาษาไทย</a></td>
                            <td><a href="https://www.chp.gov.hk/files/pdf/prevent_pneumonia_urdu.pdf" target="_blank">Pakistan - اردو   </a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="fcv--table">
                    <h3 class="fcv--table-header">Guidelines for Good Handwashing and Use Mask Properly</h3>
                    <table class="content-table">
                        <tbody>
                            <tr>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34122.html?page=1" target="_blank">Indonesia - Bahasa Indonesia</a></td>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34113.html?page=1" target="_blank">India - हिन्दी</a> </td>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34126.html?page=1" target="_blank">Nepal - नेपाली</a></td>
                            </tr>
                                <tr>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34134.html" target="_blank">Philippines - Tagalog</a></td>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34138.html" target="_blank">Thailand - ภาษาไทย</a></td>
                                <td><a href="https://www.chp.gov.hk/tc/wapdf/34132.html" target="_blank">Pakistan - اردو  </a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>

</div>
<!-- FCV POPUP :: [e] -->

<script>
    $(document).ready(function() {
        var swiper = new Swiper('.index_slider .swiper-container', {
            navigation: {
                nextEl: '.index_slider .swiper-button-next',
                prevEl: '.index_slider .swiper-button-prev',
            },
            pagination: {
                clickable: true,
                el: '.index_slider .swiper-pagination',
            },
        });
        var swiper = new Swiper('.article_slider .display-desktop .swiper-container', {
            slidesPerView: 3,
            spaceBetween: 40,
            slidesPerGroup: 3,
            breakpoints: {
                // 480: {

                // },
                1350: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                    slidesPerGroup: 3,
                }
            },
            navigation: {
                nextEl: '.article_slider .display-desktop .swiper-button-next',
                prevEl: '.article_slider .display-desktop .swiper-button-prev',
            },
        });
        var swiper = new Swiper('.article_slider .display-mobile .swiper-container', {
            pagination: {
                clickable: true,
                el: '.article_slider .display-mobile .swiper-pagination',
            },
        });

        setTimeout(function(){
                $("#fvc_popup").fadeIn();
                $("body").addClass("no-scroll");
            }
            ,2500);

        $(".fcv_modal-close, .fcv_modal-backdrop").on('click', function(){
            $("body").removeClass("no-scroll");
            $("#fvc_popup").hide();
        })
    });
</script>

<?php
include_once './templates/footer.php';
?>