<?php


$title = "Race Relations Unit - home";
$page_title = "";
$group_title = "Style Guide";



$revisionDate = '19 March 2021';
// $revisionDate = date('d F Y', strtotime('03/19/2021'));

// $with_highhighlight  = 1;


include_once '../include/config.inc.php';




require_once '../include/Breadcrumb.php'; // Include Breadcrumb class , please ensure import the file with the correct path
require_once '../include/TabMenu.php'; // Include TabMenu class , please ensure import the file with the correct path

$breadcrumb = new Breadcrumb("en", $group_title);
$breadcrumb->addBreadcrumb("Breadcrumb 1",  "#");
$breadcrumb->addBreadcrumb("Breadcrumb 2",  "#");
$breadcrumb->addBreadcrumb("Breadcrumb 3",  "#");


$tabmenu = new TabMenu($page_title);
$tabmenu->addTabMenu("Page Tab 1", "#", "active");
$tabmenu->addTabMenu("Page Tab 2", "#");
$tabmenu->addTabMenu("Page Tab 3 ", "#");
$tabmenu->addTabMenu("Page Tab 4 ", "#");

/*
!The above code should place before the header

Breadcrumb part:

$breadcrumb = new Breadcrumb("en", $group_title);
The first parameter define the language of the home page
The second parameter is the big title below the breadcrumb

$breadcrumb->addBreadcrumb("Breadcrumb 1",  "#");





TabMenu part:
It is not required on all the page.





 <div class="page-header">
    <div class="site-wrapper">
        <div class="breadcrumb">
            <a class="breadcrumb-item" href="./">Home</a>
            <a class="breadcrumb-item">Breadcrumb 1</a>
            <a class="breadcrumb-item">Breadcrumb 2</a>
            <a class="breadcrumb-item">Breadcrumb 3</a>
        </div>
        <div class="page-title"> Title </div>
    </div>
</div>

<div class="page-tab_list_wrapper">
    <div class="site-wrapper">
        <div class="page-tab_trigger"></div>
        <div class="page-tab_curr">Page Tab 1</div>
        <div class="page-tab_list">
            <a class="page-tab_list_item active" href="./content1.php">Page Tab 1</a>
            <a class="page-tab_list_item">Page Tab 2</a>
            <a class="page-tab_list_item" href="./content3.php">Page Tab 3</a>
            <a class="page-tab_list_item" href="./content4.php">Page Tab 4</a>
        </div>
    </div>
</div>


*/


include_once './templates/header.php';
?>




<div class="page-content">
    <div class="site-wrapper">
        <h1>H1 - Heading 1 </h1>
        <div class="content-section">
            <h2>H2 - Heading 2 </h2>
            <h3>H3 - Heading 3 </h3>
            <div class="content-text content-style">
                <p> &lt;p&gt; - Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
        </div>



        <!-- Content-card Start:
            Requirement:
            1. Inside the div.content-section and  div.content-text;
            2. add the a tag with  content-card class
            -->
        <div class="content-section same-row">
            <div class="content-card-container">
                <a class="content-card" href="#">Content Box 1</a>
                <a class="content-card" href="#">Content Box 2</a>
                <a class="content-card" href="#">Content Box 3</a>
                <a class="content-card" href="#">Content Box 4</a>
            </div>
        </div>
        <!-- Content-card End  -->





        <!-- Paragraph with right image:
             Requirement:
            1. Inside the div.content-section;
            2. Add the div.content-text firstly.
            3. Add the div.content-img right after the content-text.
            4. Add a div.content-img_single_item
            5. Add the img
            6. Add div.content-img_caption after the img
        -->
        <div class="content-section">
            <h2>Paragraph with right image</h2>
            <div class="content-text-with-img content-style">
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
            <div class="content-img">
                <div class="content-img_single_item">
                    <img  src="<?php echo $root_path; ?>images/test4.jpg" alt="">
                    <div class="content-img_caption"> Image caption </div>
                </div>
            </div>
        </div>
        <!--  Paragraph with right image End  -->



        <!-- Paragraph with left image:
             Requirement:
            1. Inside the div.content-section;
            2. Place the div.content-img firstly.
            3. Place the div.content-text right after the content-img.
            4. Add a div.content-img_single_item
            5. Add the img
            6. Add div.content-img_caption after the img
        -->
        <div class="content-section">
            <h2>Paragraph with left image</h2>
            <div class="content-img">
                <div class="content-img_single_item">
                    <img alt="" src="<?php echo $root_path; ?>images/test4.jpg">
                    <div class="content-img_caption">Image caption</div>
                </div>
            </div>
            <div class="content-text-with-img content-style">
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
        </div>
        <!--  Paragraph with left image End  -->




        <!-- Paragraph with right image:
            Same as "Paragraph with right image".
             Requirement:
            1. Inside the div.content-section;
            2. Place the div.content-text firstly.
            3. Place the div.content-img right after the content-text.
            4. Add a div.content-img_item
            5. Add the img
            6. Add div.content-img_caption after the img
            7. Max: two div.content-img_item
        -->
        <div class="content-section">
            <h2>Paragraph with two image</h2>
            <div class="content-text-with-img content-style">
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
            <div class="content-img">
                <div class="content-img_item">
                    <img alt="" src="<?php echo $root_path; ?>images/test4.jpg">
                    <div class="content-img_caption"> Image caption </div>
                </div>
                <div class="content-img_item">
                    <img alt="" src="<?php echo $root_path; ?>images/test5.jpg">
                    <div class="content-img_caption">Image caption</div>
                </div>
            </div>
        </div>
        <!--  Paragraph with left image End  -->





        <div class="content-section">
            <h2>Button</h2>
            <div class="content-text content-style">


                <!-- btn:
                    Requirement:
                    1. On the <a> tag, add the btn class ;
                    -->
                <p><a class="btn " href="#" title="Read More">Read More</a></p>
                <!--  btn End  -->





            </div>
        </div>

        <div class="content-section">
            <h2>Hyperlink</h2>
            <div class="content-text content-style">

                <!-- Simple <a> link -->
                <p><a href="http://www.had.gov.hk/en/public_forms/forms.htm" target="_blank">http://www.had.gov.hk/en/public_forms/forms.htm </a></p>

            </div>
        </div>


        <!--     Reponsive-table:
             Requirement:
            1. Inside the div.content-section;
            2. Add the div.reponsive-table firstly.
            3. Add <table> with a "content-table" class
        -->

        <div class="content-section">
            <h2>Table</h2>
            <div class="reponsive-table">
                <table class="content-table">
                    <thead>
                        <tr>
                            <th>Header 1</th>
                            <th>Header 2</th>
                            <th>Header 3</th>
                            <th>Header 4</th>
                            <th>Header 5</th>
                            <th>Header 6</th>
                            <th>Header 7</th>
                            <th>Header 8</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Column 1</td>
                            <td>Column 2</td>
                            <td>Column 3</td>
                            <td>Column 4</td>
                            <td>Column 5</td>
                            <td>Column 6</td>
                            <td>Column 7</td>
                            <td>Column 8</td>
                        </tr>
                        <tr>
                            <td>Column 1</td>
                            <td>Column 2</td>
                            <td>Column 3</td>
                            <td>Column 4</td>
                            <td>Column 5</td>
                            <td>Column 6</td>
                            <td>Column 7</td>
                            <td>Column 8</td>
                        </tr>
                        <tr>
                            <td>Column 1</td>
                            <td>Column 2</td>
                            <td>Column 3</td>
                            <td>Column 4</td>
                            <td>Column 5</td>
                            <td>Column 6</td>
                            <td>Column 7</td>
                            <td>Column 8</td>
                        </tr>
                        <tr>
                            <td>Column 1</td>
                            <td>Column 2</td>
                            <td>Column 3</td>
                            <td>Column 4</td>
                            <td>Column 5</td>
                            <td>Column 6</td>
                            <td>Column 7</td>
                            <td>Column 8</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="content-section">
            <h2>Table with reponsive shadow</h2>
            <div class="reponsive-shadow-table-wrapper">
                <div class="reponsive-shadow-table">
                    <table class="content-table-white">
                        <thead>
                            <tr>
                                <th>Header 1</th>
                                <th>Header 2</th>
                                <th>Header 3</th>
                                <th>Header 4</th>
                                <th>Header 5</th>
                                <th>Header 6</th>
                                <th>Header 7</th>
                                <th>Header 8</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Column 1</td>
                                <td>Column 2</td>
                                <td>Column 3</td>
                                <td>Column 4</td>
                                <td>Column 5</td>
                                <td>Column 6</td>
                                <td>Column 7</td>
                                <td>Column 8</td>
                            </tr>
                            <tr>
                                <td>Column 1</td>
                                <td>Column 2</td>
                                <td>Column 3</td>
                                <td>Column 4</td>
                                <td>Column 5</td>
                                <td>Column 6</td>
                                <td>Column 7</td>
                                <td>Column 8</td>
                            </tr>
                            <tr>
                                <td>Column 1</td>
                                <td>Column 2</td>
                                <td>Column 3</td>
                                <td>Column 4</td>
                                <td>Column 5</td>
                                <td>Column 6</td>
                                <td>Column 7</td>
                                <td>Column 8</td>
                            </tr>
                            <tr>
                                <td>Column 1</td>
                                <td>Column 2</td>
                                <td>Column 3</td>
                                <td>Column 4</td>
                                <td>Column 5</td>
                                <td>Column 6</td>
                                <td>Column 7</td>
                                <td>Column 8</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
<!-- Resposive -->
        <script>

            $(document).ready(function() {
                var scrollLeft = $(".reponsive-shadow-table").scrollLeft();
                var resizeTimer;
                var scrollTimer;
                var tableWidth = $(".reponsive-shadow-table table").width();
                var tableViewWidth = $(".reponsive-shadow-table").width();

                if(scrollLeft == 0){
                    $(".reponsive-shadow-table-wrapper").addClass("on-start");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-start");
                }

                if(scrollLeft >= tableWidth - tableViewWidth){
                    $(".reponsive-shadow-table-wrapper").addClass("on-end");
                }else{
                    $(".reponsive-shadow-table-wrapper").removeClass("on-end");
                }


                $(window).on('resize', function(e) {

                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        var scrollLeft = $(".reponsive-shadow-table").scrollLeft();

                        if(scrollLeft == 0){
                            $(".reponsive-shadow-table-wrapper").addClass("on-start");
                        }else{
                            $(".reponsive-shadow-table-wrapper").removeClass("on-start");
                        }

                        if(scrollLeft == tableWidth - tableViewWidth){
                            $(".reponsive-shadow-table-wrapper").addClass("on-end");
                        }else{
                            $(".reponsive-shadow-table-wrapper").removeClass("on-end");
                        }

                    }, 250);

                });

                $(".reponsive-shadow-table").on('scroll', function(e) {

                    clearTimeout(scrollTimer);
                    scrollTimer = setTimeout(function() {
                        var scrollLeft = $(".reponsive-shadow-table").scrollLeft();

                        if(scrollLeft == 0){
                            $(".reponsive-shadow-table-wrapper").addClass("on-start");
                        }else{
                            $(".reponsive-shadow-table-wrapper").removeClass("on-start");
                        }

                        tableWidth = $(".reponsive-shadow-table table").width();
                        tableViewWidth = $(".reponsive-shadow-table").width();

                        if(scrollLeft >= tableWidth - tableViewWidth){
                            $(".reponsive-shadow-table-wrapper").addClass("on-end");
                        }else{
                            $(".reponsive-shadow-table-wrapper").removeClass("on-end");
                        }

                    }, 150);

                });
            });

        </script>

        <!-- Reponsive-table end -->



        <!-- <div class="index-section">
            <div class="site-wrapper">
                <h2 class="text-center">News Annocement</h2>
                <div class="section-news-list">
                    <div class="section-news-item">
                        <a href="#">
                            <div class="section-news-item_date">9 Dec 2020</div>
                            <div class="section-news-item_title">News Description</div>
                        </a>
                    </div>
                    <div class="section-news-item">
                        <a href="#">
                            <div class="section-news-item_date">8 Dec 2020</div>
                            <div class="section-news-item_title">News Description</div>
                        </a>
                    </div>
                </div>
                <div class="section-news-btn"><a href="#" class="btn">All News</a></div>
            </div>
        </div> -->


        <!--    Video Slider:
             Requirement:
            1. Inside the div.content-section and content-slider;
            2. Add swiper-button-prev  and swiper-button-next if required
            3. Add the div.swiper-container.
            4. Add the div.swiper-wrapper
            5. Add numerous div.swiper-slide,
            6. Within the swiper-slide, add div.content-slider_video with the youtube iframe and div.content-slider_title with the video title
            7. Lastly add the div.swiper-pagination to show the slider pagination
        -->


        <div class="content-section content-slider">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <h2>Video Slider</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="content-slider_video"><iframe width="560" height="315" src="https://www.youtube.com/embed/ynxTbIF62Aw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                        <div class="content-slider_title">Stay Safe Stay Away from Unlicensed Guesthouses</div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>


        <!--    Video Slider end         -->


        <div>
            <h2>Section</h2>
            <div class="content-group">
                <div class="content-group-title">Title</div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <br />
                        Lorem ipsum dolor sit amet consectetuer adipiscing elit.<br />
                        Lorem ipsum dolor sit amet consectetuer adipiscing elit.<br />
                        <br />
                    </div>
                </div>
            </div>
        </div>






        <div>
            <h2>Accordion</h2>


            <!--    Simple accordion:
             Requirement:
            1. Inside the div.content-group accordion
            2. Add a div.content-group-title as a title
            3. Add a div.content-group-text-space as the content
        -->


            <div class="content-group accordion">
                <div class="content-group-title"> Simple </div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-space">
                        <div class="content-link-block">
                            <div class="content-link-block-time"> Row 1 </div>
                            <div class="content-link-block-text"><a>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor</a></div>
                        </div>
                        <div class="content-link-block">
                            <div class="content-link-block-time"> Row 2 </div>
                            <div class="content-link-block-text">Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor </div>
                        </div>
                        <div class="content-link-block">
                            <div class="content-link-block-time"> Row 3 </div>
                            <div class="content-link-block-text"> Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor </div>
                        </div>
                    </div>
                </div>
            </div>



            <!--   Color Table accordion:
             Requirement:
            1. Inside the div.content-group accordion
            2. Add a div.content-group-title as a title
            3. Add a div.content-group-text-group to divide the row
            4. Add a div.content-group-text-space as the content
        -->

            <div class="content-group accordion">
                <div class="content-group-title"> Color Table accordion </div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-price-block">
                                <div class="content-price-block-text"> Row 1 </div>
                                <div class="content-price-block-price"> Price </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-price-block">
                                <div class="content-price-block-text"> Row 2 </div>
                                <div class="content-price-block-price">Price </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-price-block">
                                <div class="content-price-block-text"> Row 3 </div>
                                <div class="content-price-block-price">Price</div>
                            </div>
                        </div>
                    </div>
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-price-block">
                                <div class="content-price-block-text"> Row 4 </div>
                                <div class="content-price-block-price">Price</div>
                            </div>
                        </div>
                    </div>
                    <?php
                    // .content-price-block-text define the width
                    ?>
                </div>
            </div>


            <!--    Second accordion:
             Requirement:
            1. Inside the div.content-group accordion
            2. Add a div.content-group-title as a title
            3. Add a div.content-group-text-group to divide the row
            4. Add a div.content-group-text-space as the content
            5. Add a div.content-accordion-block with div.content-accordion-block-title and div.content-accordion-block-text
        -->

            <div class="content-group accordion">
                <div class="content-group-title"> Second Accordion </div>
                <div class="content-group-text content-style">
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-accordion-block">
                                <div class="content-accordion-block-title"> Question 1 </div>
                                <div class="content-accordion-block-text">
                                    Answer 1 (Listing)
                                    <ol type="a">
                                        <li>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque nascetur ridiculus mus. Lorem ipsum dolor sit amet consectetuer adipiscing elit; and</li>
                                        <li>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque nascetur ridiculus mus. Lorem ipsum dolor sit amet consectetuer adipiscing elit.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-group-text-group">
                        <div class="content-group-text-space">
                            <div class="content-accordion-block">
                                <div class="content-accordion-block-title">Question 2 </div>
                                <div class="content-accordion-block-text"> Lorem ipsum dolor sit amet consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque nascetur ridiculus mus. Lorem ipsum dolor sit amet consectetuer adipiscing elit. </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>

    </div>
</div>

<?php
include_once './templates/footer.php';
?>