<?php

$lang = "en";
// $revisionDate='03/16/2021';
$reviewDate = '04/01/2012';

if (!isset($title)) {
    $title = "Race Relations Unit ";
}


// echo $root_path;


// exit();

?>



<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no" />
    <title><?php echo htmlspecialchars($title); ?></title>

    <link rel="shortcut icon" href="<?php echo $root_path; ?>/images/favicon.png" />

    <link rel="preconnect" href="//fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@300;400;500;700;900&display=swap" rel="stylesheet">

    <script type="text/javascript" src="<?php echo $root_path; ?>libs/jquery/jquery-213.min.js"></script>
    <script type="text/javascript" src="<?php echo $root_path; ?>libs/jquery/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.2/css/fixedColumns.dataTables.min.css"/>

    <!-- <link rel="stylesheet" href="<?php echo $root_path; ?>libs/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />

    <link href="<?php echo $root_path; ?>libs/hamburgers/hamburgers.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $root_path; ?>libs/swiper-4.5.3/css/swiper.min.css">
    <script type="text/javascript" src="<?php echo $root_path; ?>libs/swiper-4.5.3/js/swiper.min.js"></script>

    <!-- <link rel="stylesheet" href="libs/swiper-6.4.15/swiper-bundle.min.css" />
        <script src="libs/swiper-6.4.15/swiper-bundle.min.js"></script> -->

    <link rel="stylesheet" href="<?php echo $root_path; ?>style/main_fn.css?t=<?php echo time(); ?>" />
    <script type="text/javascript" src="<?php echo $root_path; ?>js/main_fn.js?t=<?php echo time(); ?>"></script>

    <link rel="stylesheet" href="<?php echo $root_path; ?>style/main_content.css?t=<?php echo time(); ?>" />
    <link rel="stylesheet" href="<?php echo $root_path; ?>style/main_search.css?t=<?php echo time(); ?>" />

    <link rel="stylesheet" href="<?php echo $root_path; ?>style/paint.css?t=<?php echo time(); ?>" />
</head>

<body>
    <div id="web-top">
        <header>
            <?php if (isset($with_highhighlight) && $with_highhighlight) : ?>
                <!-- <div class="top-highlight">
                    <div class="site-wrapper">
                        <div class="top-highlight_close"></div>
                        <div>
                            <div class="top-highlight-row">
                                <a class="top-highlight-item large">
                                    <div class="top-highlight-item-img"><img " src=" <?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></div>
                                    <div class="top-highlight-item-title">年滿30歲可接種新冠疫苗</div>
                                </a>
                            </div>
                            <div class="top-highlight-row">
                                <a class="top-highlight-item small">
                                    <div class="top-highlight-item-img"><img " src=" <?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></div>
                                    <div class="top-highlight-item-title">死亡個案與疫苗接種無直接關係</div>
                                </a>
                                <a class="top-highlight-item small">
                                    <div class="top-highlight-item-img"><img " src=" <?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></div>
                                    <div class="top-highlight-item-title">死亡個案與疫苗接種無直接關係</div>
                                </a>
                                <a class="top-highlight-item small">
                                    <div class="top-highlight-item-img"><img " src=" <?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></div>
                                    <div class="top-highlight-item-title">死亡個案與疫苗接種無直接關係</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->

            <?php endif; ?>

            <div class="site-nav">
                <div class="site-wrapper">
                    <div class="header-bar">
                        <a class="header-bar-left" href="<?php echo  $root_path; ?>">
                            <div class="header-logo_img"><img alt="logo" src="<?php echo $root_path; ?>images/logo.svg"></div>
                            <div class="header-logo_text">
                                <div class="desktop">
                                    <div class="header-logo_text-title">Race Relations Unit</div>
                                    <div class="header-logo_text-text">Home Affairs Department<br />The Government of the Hong Kong Special Administrative Region</div>
                                </div>
                                <div class="mobile">
                                    <img alt="department" src="<?php echo $root_path; ?>images/logo-name.svg">
                                </div>
                            </div>
                        </a>
                        <div class="header-bar-right">
                            <div class="desktop">
                                <div class="header-tools">
                                    <div class="header-tools_item btn-search"><img alt="search" src="<?php echo $root_path; ?>images/search.svg"></div>
                                    <div class="header-tools_item btn-print"><img alt="print" src="<?php echo $root_path; ?>images/print.svg"></div>
                                    <div class="header-tools_item"><a href="javascript:changeLang('/en/', '/tc/')">繁</a></div>
                                    <div class="header-tools_item"><a href="javascript:changeLang('/en/', '/sc/')">簡</a></div>
                                    <div class="header-tools_item">Text Size</div>
                                </div>
                            </div>
                            <div class="desktop">
                                <div class="other-lang">
                                    <div class="other-lang-item"><a href="#">Bahasa Indonesia</a></div>
                                    <div class="other-lang-item"><a href="#">हिन्दी</a></div>
                                    <div class="other-lang-item"><a href="#">नेपाली</a></div>
                                    <div class="other-lang-item"><a href="#">Tagalog</a></div>
                                    <div class="other-lang-item"><a href="#">ภาษาไทย</a></div>
                                    <div class="other-lang-item"><a href="#">اردو</a></div>
                                </div>
                            </div>

                            <div class="mobile">
                                <div class="mobile_header-tools">
                                    <div class="mobile_header-tools_item btn-search"><img alt="search" src="<?php echo $root_path; ?>images/search.svg"></div>
                                    <div class="mobile_header-tools_item"><img alt="lang" src="<?php echo $root_path; ?>images/lang.svg">
                                        <div class="lang_menu">
                                            <div class="lang_menu_item"><a href="javascript:changeLang('/en/', '/tc/')">繁</a></div>
                                            <div class="lang_menu_item"><a href="javascript:changeLang('/en/', '/sc/')">簡</a></div>
                                        </div>
                                    </div>
                                    <div class="mobile_header-tools_item menu-trigger">
                                        <div class="hamburger hamburger--vortex">
                                            <div class="hamburger-box">
                                                <div class="hamburger-inner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bar-full desktop">
                    <div class="header-nav">
                        <div class="header-nav_item has-sub">
                            <a class="header-nav_item-link no-underline" href="<?php echo $BASE_URL . $lang; ?>/aboutus/race-relations-unit.php">About Us</a>
                            <div class="header-nav_sub">
                                <div class="header-nav_sub-container">
                                    <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/the-committee-on-the-promotion.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">The Committee on the Promotion of Racial Harmony</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/ethnices-minorities-forum.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Ethnics Minorities Forum</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/race-relations-unit.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Race Relations Unit</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-nav_item has-sub">
                            <a class="header-nav_item-link no-underline">Programmes and Services</a>
                            <div class="header-nav_sub">
                                <div class="header-nav_sub-container">
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/support-service-centres.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Support Service Centres for Ethnic Minorities</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/community-support-teams.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Community Support Teams</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/district-based-programmes.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">District-based Programmes for Racial Harmony</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/language-programme.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Language Programme</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/cross-cultural-learning-youth-programme.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Cross-cultural Learning Youth Programme</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/mobile-information-service.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Mobile Information Service</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/radio-programme.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Radio Programme</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/eduction-talks-on-racial-harmony.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Education Talks on Racial Harmony</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/programmes/harmony-scholarships-scheme.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Harmony Scholarships Scheme</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-nav_item has-sub">
                            <a class="header-nav_item-link no-underline">Information Centre</a>
                            <div class="header-nav_sub">
                                <div class="header-nav_sub-container">
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/demographics.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Demographics</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/self-help-tips.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Geotechnical Engineering Office's reminder on Landslide Self-help Tips</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/health-info-for-ethnic-minorities.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Health Information on Communicable Diseases and Hygiene for Ethnic Minorities</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/health-info.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Health Information</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/publications.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Publications</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/feature-articles.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Feature Articles</div>
                                    </a>
                                    <a href="<?php echo $BASE_URL . $lang; ?>/info/useful-links.php" class="header-nav_sub-item no-underline">
                                        <div class="header-nav_sub-item_name">Useful Links</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-nav_item">
                            <a class="header-nav_item-link no-underline" href="#">Contact Us</a>
                        </div>

                    </div>
                </div>
            </div>

        </header>
        <div class="mobile_header-nav_wrapper">
                <div class="mobile_header-nav">
                    <div class="site-wrapper">
                        <div class="mobile_header-nav_item has-sub">
                            <a class="mobile_header-nav-link no-underline">About Us</a>
                            <div class="mobile_header-nav_sub">
                                <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/the-committee-on-the-promotion.php" class="mobile_header-nav_sub-item no-underline">The Committee on the Promotion of Racial harmony</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/ethnices-minorities-forum.php" class="mobile_header-nav_sub-item no-underline">Ethnics Minorities Forum</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/aboutus/race-relations-unit.php" class="mobile_header-nav_sub-item no-underline">Race Relations Unit</a>
                            </div>
                        </div>
                        <div class="mobile_header-nav_item has-sub">
                            <a class="mobile_header-nav-link no-underline">Programmes and Services</a>
                            <div class="mobile_header-nav_sub">
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/support-service-centres.php" class="mobile_header-nav_sub-item no-underline">Support Service Centres for Ethnic Minorities</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/community-support-teams.php" class="mobile_header-nav_sub-item no-underline">Community Support Teams</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/district-based-programmes.php" class="mobile_header-nav_sub-item no-underline">District-based Programmes for Racial Harmony</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/language-programme.php" class="mobile_header-nav_sub-item no-underline">Language Programme</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/cross-cultural-learning-youth-programme.php" class="mobile_header-nav_sub-item no-underline">Cross-cultural Learning Youth Programme</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/mobile-information-service.php" class="mobile_header-nav_sub-item no-underline">Mobile Information Service</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/radio-programme.php" class="mobile_header-nav_sub-item no-underline">Radio Programme</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/eduction-talks-on-racial-harmony.php" class="mobile_header-nav_sub-item no-underline">Education Talks on Racial Harmony</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/programmes/harmony-scholarships-scheme.php" class="mobile_header-nav_sub-item no-underline">Harmony Scholarships Scheme</a>
                            </div>
                        </div>
                        <div class="mobile_header-nav_item has-sub">
                            <a class="mobile_header-nav-link no-underline">Information Centre</a>
                            <div class="mobile_header-nav_sub">
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/demographics.php" class="mobile_header-nav_sub-item no-underline">Demographics</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/self-help-tips.php" class="mobile_header-nav_sub-item no-underline">Geotechnical Engineering Office's reminder on Landslide Self-help Tips</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/health-info-for-ethnic-minorities.php" class="mobile_header-nav_sub-item no-underline">Health Information on Communicable Diseases and Hygiene for Ethnic Minorities</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/health-info.php" class="mobile_header-nav_sub-item no-underline">Health Information</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/publications.php" class="mobile_header-nav_sub-item no-underline">Publications</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/feature-articles.php" class="mobile_header-nav_sub-item no-underline">Feature Articles</a>
                                <a href="<?php echo $BASE_URL . $lang; ?>/info/useful-links.php" class="mobile_header-nav_sub-item no-underline">Useful Links</a>
                            </div>
                        </div>
                        <div class="mobile_header-nav_item">
                            <a href="#" class="mobile_header-nav-link no-underline">Contact Us</a>
                        </div>
                        <div class="other-tools">
                            <div class="other-tools_item btn-print"><img alt="print" src="<?php echo $root_path; ?>images/print--white.svg"></div>
                        </div>
                        <div class="other-lang">
                            <div class="other-lang-item"><a href="#">Bahasa Indonesia</a></div>
                            <div class="other-lang-item"><a href="#">हिन्दी</a></div>
                            <div class="other-lang-item"><a href="#">नेपाली</a></div>
                            <div class="other-lang-item"><a href="#">Tagalog</a></div>
                            <div class="other-lang-item"><a href="#">ภาษาไทย</a></div>
                            <div class="other-lang-item"><a href="#">اردو</a></div>
                        </div>
                    </div>
                </div>
            </div>







        <?php
        if (isset($breadcrumb)) {
            echo $breadcrumb->getBreadCrumb(true);
        }

        if (isset($tabmenu)) {
            echo $tabmenu->getTabMenu();
        }


        ?>