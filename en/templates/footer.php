            <div class="index-section">
                <div class="site-wrapper">
                    <div class="other_site_slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_2.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_1.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_2.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_3.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/cat_en.jpg"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_2.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_1.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_2.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/bannel_3.gif"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/cat_en.jpg"></a></div>
                                <div class="swiper-slide"><a href="" target="_blank"><img alt="" src="<?php echo $root_path; ?>images/Banner_iAMSmart 85x50.jpg"></a></div>
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>

            <script>
                function changeLang(e, t) {
                    var i = window.location,
                        n = i.toString(),
                        s = i.toString();
                    s = s.replace(e, t), window.location = s == n ? t : s
                }



                $(document).ready(function() {
                    var swiper = new Swiper('.other_site_slider .swiper-container', {
                        loop: true,
                        autoplay: {
                            delay: 7500,
                        },
                        slidesPerGroup: 5,
                        slidesPerView: 5,
                        spaceBetween: 20,
                        breakpoints: {
                            // 350: {

                            // },
                            520: {
                                slidesPerGroup: 2,
                                slidesPerView: 2,
                                spaceBetween: 20
                            },
                            760: {
                                slidesPerGroup: 3,
                                slidesPerView: 3,
                                spaceBetween: 20
                            },
                            960: {
                                slidesPerGroup: 4,
                                slidesPerView: 4,
                                spaceBetween: 20
                            }
                        },
                        navigation: {
                            nextEl: '.other_site_slider .swiper-button-next',
                            prevEl: '.other_site_slider .swiper-button-prev',
                        },
                        pagination: {
                            clickable: true,
                            el: '.other_site_slider .swiper-pagination',
                        },
                    });
                });
            </script>
            </div>
            <div class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
            <div class="bottom-menu-wrapper">
                <div class="site-wrapper">
                    <div class="bottom-menu">
                        <div class="bottom-menu_row">
                            <div class="bottom-menu_row-title">About Us</div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/aboutus/the-committee-on-the-promotion.php">The Committee on the Promotion of Racial harmony</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/aboutus/ethnices-minorities-forum.php">Ethnics Minorities Forum</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/aboutus/race-relations-unit.php">Race Relations Unit</a></div>
                        </div>
                        <div class="bottom-menu_row">
                            <div class="bottom-menu_row-title">Programmes and Services</div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/support-service-centres.php">Support Service Centres for Ethnic Minorities</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/community-support-teams.php">Community Support Teams</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/district-based-programmes.php">District-based Programmes for Racial Harmony</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/language-programme.php">Language Programme</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/cross-cultural-learning-youth-programme.php">Cross-cultural Learning Youth Programme</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/mobile-information-service.php">Mobile Information Service</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/radio-programme.php">Radio Programme</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/eduction-talks-on-racial-harmony.php">Education Talks on Racial Harmony</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/programmes/harmony-scholarships-scheme.php">Harmony Scholarships Scheme</a></div>
                        </div>
                        <div class="bottom-menu_row">
                            <div class="bottom-menu_row-title">Information Centre</div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/demographics.php">Demographics</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/self-help-tips.php">Geotechnical Engineering Office's reminder on Landslide Self-help Tips</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/health-info-for-ethnic-minorities.php">Health Information on Communicable Diseases and Hygiene for Ethnic Minorities</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/health-info.php">Health Information</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/publications.php">Publications</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/feature-articles.php">Feature Articles</a></div>
                            <div class="bottom-menu_row-item"><a href="<?php echo $BASE_URL . $lang; ?>/info/useful-links.php">Useful Links</a></div>

                        </div>
                        <div class="bottom-menu_row">
                            <div class="bottom-menu_row-title">Contact Us</div>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <div class="site-wrapper">
                    <div class="bottom-logo">
                        <div class="bottom-logo_item"><img src="<?php echo $root_path; ?>images/wcag2AA.gif" style="width: 90px;"></div>
                        <div class="bottom-logo_item"><img src="<?php echo $root_path; ?>images/web_all_logo.png" style="width: 105px;"></div>
                        <div class="bottom-logo_item"><img src="<?php echo $root_path; ?>images/brandhk.png" style="width: 140px;"></div>
                    </div>
                    <?php if (isset($revisionDate)) : ?>
                        <div class="bottom-date">Last revision date : <?php echo $revisionDate; ?></div>
                    <?php endif; ?>
                    <div class="bottom-nav">
                        <div class="bottom-nav_item">2009 ©</div>
                        <div class="bottom-nav_item"><a href="#">Sitemap</a></div>
                        <div class="bottom-nav_item"><a href="#">Important Notices</a></div>
                        <div class="bottom-nav_item"><a href="#">Privacy Policy</a></div>
                    </div>
                </div>
            </footer>
            </body>

            </html>