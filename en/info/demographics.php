<?php



$title = "Race Relations Unit - Information Centre "; // Web Title on Tab

$group_title = "Information Centre"; // Control Breadcrumb Title
$page_title = "The Demographics : Ethnic Groups"; // Control Which tab is active and set the page title
$revisionDate = '19 March 2021'; // Last revision date on Footer

$current_path = pathinfo(__FILE__, PATHINFO_BASENAME);

include_once '../../include/config.inc.php'; // Include $root_path setting

include_once './page_header.inc.php'; //Include page-header , breadcrumb





include_once '../templates/header.php'; // Include Header
// include_once './page_header.inc.php'; //Include page-header , breadcrumb
// include_once './page_tab_list_wrapper.inc.php'; //Include the tab

?>

<div class="page-content">
    <div class="site-wrapper">
        <h2>The Demographics : Ethnic Groups</h2>
        <div class="content-text content-style">
            <p>Hong Kong is a largely homogenous society, with about 92% of its people being Chinese (ethnically speaking, Han Chinese). The 2016 Population By-census found (by way of self-identification) that there were about 584,383 non-Chinese people in Hong Kong, or about 8% of the population. Not all members of the non-Chinese groups are permanently settled in Hong Kong.</p>
            <p>Hong Kong's principal ethnic minorities are -</p>
            <div class="reponsive-table">
                <table class="content-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>(2016 Population By-census)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Ethnicity (Self-identification)</td>
                            <td>Total number</td>
                        </tr>
                        <tr>
                            <td>Indonesian</td>
                            <td>153 299</td>
                        </tr>
                        <tr>
                            <td>Filipino</td>
                            <td>184 081</td>
                        </tr>
                        <tr>
                            <td>White</td>
                            <td>58 209</td>
                        </tr>
                        <tr>
                            <td>Indian</td>
                            <td>36 462</td>
                        </tr>
                        <tr>
                            <td>Pakistani</td>
                            <td>18 094</td>
                        </tr>
                        <tr>
                            <td>Nepalese</td>
                            <td>25 472</td>
                        </tr>
                        <tr>
                            <td>Japanese</td>
                            <td>9 976</td>
                        </tr>
                        <tr>
                            <td>Thai</td>
                            <td>10 215</td>
                        </tr>
                        <tr>
                            <td>Other Asian</td>
                            <td>19 589</td>
                        </tr>
                        <tr>
                            <td>Others</td>
                            <td>68 986</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="content-text content-style text-center margin0auto">
                <img src="<?php echo $BASE_URL?>images/graph-ethnic_groups.png" alt="">
            </div>


            <p>Further details about the characteristics of ethnic minorities are available in the publication entitled <a href="#">"Hong Kong 2016 Population By-census Thematic Report：Ethnic Minorities"</a> which has been published by Census and Statistics Department.</p>




        </div>

    </div>
</div>

<?php
include_once '../templates/footer.php';
?>