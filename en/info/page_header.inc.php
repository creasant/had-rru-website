<?php

require_once '../../include/Breadcrumb.php'; // Include Breadcrumb class
require_once '../../include/TabMenu.php'; // Include TabMenu class
$breadcrumb = new Breadcrumb("en", $group_title);
$breadcrumb->addBreadcrumb("Information Centre",  "demographics.php");
$breadcrumb->addBreadcrumb($page_title,  $current_path);
