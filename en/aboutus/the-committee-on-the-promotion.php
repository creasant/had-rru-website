<?php



$title = "Race Relations Unit - About us "; // Web Title on Tab

$group_title = "About Us"; // Control Breadcrumb Title
$page_title = "The Committee on the Promotion of Racial Harmony"; // Control Which tab is active and set the page title
$revisionDate = '19 March 2021'; // Last revision date on Footer
$current_path = pathinfo(__FILE__, PATHINFO_BASENAME);

include_once '../../include/config.inc.php'; // Include $root_path setting

include_once './page_header.inc.php'; //Include page-header , breadcrumb





include_once '../templates/header.php'; // Include Header
// include_once './page_header.inc.php'; //Include page-header , breadcrumb
// include_once './page_tab_list_wrapper.inc.php'; //Include the tab

?>

<div class="page-content">
    <div class="site-wrapper">
        <h2>The Committee on the Promotion of Racial Harmony</h2>
        <div class="content-text content-style">
            <p>Appointed by the Secretary for Home Affairs</p>

            <h3 class="subtitle">Chairman</h3>
            <p>Deputy Director of Home Affairs (2)</p>

            <h3 class="subtitle">Non-official members</h3>
            <ul>
                <li>Mr Mohamed Ibramsa Sikkander Batcha, MH</li>
                <li>Ms Ping Somporn Bevan</li>
                <li>Ms Mimi Cheung Yee-may</li>
                <li>Dr Theresa Cunanan</li>
                <li>Mr Syed Ekram Elahi</li>
                <li>Ms Rita Gurung</li>
                <li>Mr Vijay Harilela</li>
                <li>Mr Avinash Chandiram Hotchandani</li>
                <li>Mr Derek Hung Chiu-wah</li>
                <li>Mr Akil Khan</li>
                <li>Ms Vivian Kong Man-wai</li>
                <li>Mrs Poonam Vijayprakash Mehta</li>
                <li>Ms Lamia Sreya Rahman</li>
                <li>Ms Rigam Rai</li>
                <li>Dr Chura Bahadur Thapa</li>
                <li>Ms Yvonne Tsui Hang-on</li>
                <li>Mr Wong Ka-chun</li>
                <li>Mr Matthew Wong Man-ho</li>
            </ul>

            <h3 class="subtitle">Official Members</h3>
            <ul>
                <li>Secretary for Constitutional and Mainland Affairs or his representative</li>
                <li>Secretary for Education or his representative</li>
                <li>Commissioner for Labour or his representative</li>
                <li>Director of Information Services or his representative</li>
            </ul>

            <h3 class="subtitle">Secretary</h3>
            <p>Senior Executive Officer (Race Relations Unit)</p>

            <h3 class="subtitle">Terms of reference</h3>
            <ol class="laprarent">
                <li>To formulate proposals for promotion of racial harmony, including provision of support services to ethnicminorities and race related public education and publicity. Such proposals should include advice on the practicalities of budget and implementation;</li>
                <li>To liaise with and assist Government departments and community organizations to promote racial harmony and equality; and</li>
                <li>To encourage all sectors of the community, particularly the ethnic minorities, actively to promote awareness of the issues entailed.</li>
            </ol>
            <h3 class="subtitle">Description of the job</h3>
            <p>The Committee has initiated a range of support services, including the cross-cultural learning youth programme, radio programmes in minority languages and Community Support Teams.</p>

            <h3 class="subtitle">Enquiry</h3>
            <p>2835 1458</p>

            <h3 class="subtitle">Committee Meeting</h3>

            <h3 class="subtitle">Meetings of the Committee on the Promotion of Racial Harmony</h3>
            <div class="table-container">
                <table id="dt-fixleft" class="stripe row-border order-column" style="width:100%">
                    <thead>
                        <tr>
                            <th class="left">Meetings of the Committee on <br/>the Promotion of Racial Harmony</th>
                            <th>Agenda</th>
                            <th>Papers</th>
                            <th>Minutes</th>
                            <th>Annex to Minutes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>30 June 2020</td>
                            <td><a class="link-pdf" href="#" ><img src="<?php echo $BASE_URL?>images/icon-pdf.svg" alt=""></a></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19 September 2019</td>
                            <td><a class="link-pdf" href="#" ><img src="<?php echo $BASE_URL?>images/icon-pdf.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-pdf" href="#" ><img src="<?php echo $BASE_URL?>images/icon-pdf.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7 March 2019</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-pdf" href="#" ><img src="<?php echo $BASE_URL?>images/icon-pdf.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>24 October 2018</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>13 March 2018</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15 September 2017</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15 February 2017</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>28 June 2016</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>14 January 2016</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>21 July 2015</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5 December 2014</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>11 July 2014</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5 December 2013</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>20 June 2013</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19 October 2012</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>13 December 2011</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>26 May 2011</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6 December 2010</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6 July 2010</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>25 February 2010</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5 November 2009</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>29 April 2009</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2 October 2008</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>14 May 2008</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>16 November 2007</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>24 April 2007</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>16 January 2007</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15 September 2006</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-ppt" href="#" ><img src="<?php echo $BASE_URL?>images/icon-ppt.svg" alt=""></a></td>
                        </tr>
                        <tr>
                            <td>10 May 2006</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>17 January 2006</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19 October 2005</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>20 July 2005</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-bundle" href="#" ><img src="<?php echo $BASE_URL?>images/icon-bundle.svg" alt=""></a></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td><a class="link-pdf" href="#" ><img src="<?php echo $BASE_URL?>images/icon-pdf.svg" alt=""></a></td>
                        </tr>
                        <tr>
                            <td>31 March 2005</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15 December 2004</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>22 September 2004</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19 July 2004</td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                            <td><a class="link-doc" href="#" ><img src="<?php echo $BASE_URL?>images/icon-doc.svg" alt=""></a></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>



        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#dt-fixleft').DataTable( {
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            ordering: false,
            info:     false,
            searching: false,
            columnDefs: [
                { width: 180, targets: 0 },
                { width: 140, targets: 1 },
                { width: 140, targets: 2 },
                { width: 140, targets: 3 },
                { width: 140, targets: 4 },
            ],
            fixedColumns:   {
                leftColumns: 1,
            },
            fixedHeader: true

        } );
    } );
</script>

<?php
include_once '../templates/footer.php';
?>