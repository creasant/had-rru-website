<?php



$title = "Race Relations Unit - About us "; // Web Title on Tab

$group_title = "About Us"; // Control Breadcrumb Title
$page_title = "Race Relations Unit"; // Control Which tab is active and set the page title
$revisionDate = '19 March 2021'; // Last revision date on Footer

$current_path = pathinfo(__FILE__, PATHINFO_BASENAME);


include_once '../../include/config.inc.php'; // Include $root_path setting

include_once './page_header.inc.php'; //Include page-header , breadcrumb





include_once '../templates/header.php'; // Include Header
// include_once './page_header.inc.php'; //Include page-header , breadcrumb
// include_once './page_tab_list_wrapper.inc.php'; //Include the tab

?>

<div class="page-content">
    <div class="site-wrapper">
        <h2>Race Relations Unit</h2>
        <div class="content-text content-style">
            <p>The Race Relations Unit (RRU) commenced operation on 17 June 2002. The Unit renders secretarial support to the Committee on the Promotion of Racial Harmony, Ethnic Minorities Forum and to provide a range of support services, either by implementing its own programmes or through sponsoring non-governmental organisations.</p>
        </div>
        <div class="content-card-container">
            <a class="content-card" href="the-committee-on-the-promotion.php">The Committee on the<br/> Promotion of Racial Harmony</a>
            <a class="content-card" href="ethnices-minorities-forum.php">Ethnic Minorities Forum</a>
        </div>

    </div>
</div>

<?php
include_once '../templates/footer.php';
?>