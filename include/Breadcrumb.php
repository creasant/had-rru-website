<?php

class Breadcrumb
{
    protected $totalItems;
    protected $group_title;
    protected $lang;

    // protected $root_path =  "http://localhost/gov_hadla/";
    protected $root_path =  "http://project24.creasant.in/gov_hadla/";

    public function __construct($lang, $group_title)
    {
        $this->totalItems = array();
        $this->lang = $lang;
        $this->group_title = $group_title;
        // array_push($this->totalItems, array(
        //     "text" => "Home",
        //     "link" => "welcome.php")
        // );
    }

    public function addBreadcrumb($text, $link = "")
    {
        array_push(
            $this->totalItems,
            array(
                "text" => $text,
                "link" => $link
            )
        );
    }

    public function getBreadcrumb($hasMainMenu = false)
    {


        $result = ' <div class="page-header">
        <div class="site-wrapper">
        <div class="breadcrumb">
        ';

        // if ($hasMainMenu) {
        // if ($this->lang == "en") {
        $result .= '<a class="breadcrumb-item" href="' .  $this->root_path . $this->lang . '">Home</a>';
        // }else {
        // }

        $last = count($this->totalItems);

        foreach ($this->totalItems as $keys => $breadcrumb) {
            $text = $breadcrumb['text'];
            $link = $breadcrumb['link'];
            $current = ( $keys !== $last -1 ) ? "" : " current";

            if ($link != "#" && $link != "") {
                $result .= ' <a class="breadcrumb-item'.$current.'" href="' . $link . '">' . $text . '</a>';
            } else {
                $result .= ' <a class="breadcrumb-item'.$current.'" >' . $text . '</a>';
            }
        }

        $result .= '    </div>
        <div class="page-title">' .   $this->group_title . '</div>
        </div> </div>
         ';



        // $result = '<ul class="breadcrumb">';
        // if ($hasMainMenu) {
        //     $result .= '<li><a href="welcome.php">Main Menu</a>
        //     </li>';
        // }
        // foreach ($this->totalItems as $breadcrumb) {
        //     $text = $breadcrumb['text'];
        //     $link = $breadcrumb['link'];
        //     if ($link != "#" && $link != "") {
        //         $result .= '<li><a href="' . $link . '">' . $text . '</a></li>';
        //     } else {
        //         $result .= '<li><span>' . $text . '</span></li>';
        //     }
        // }
        // $result .= '</ul>';
        return $result;
    }
}
