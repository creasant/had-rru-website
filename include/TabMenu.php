<?php

class TabMenu
{
    protected $totalItems;
    protected $page_title;

    public function __construct($page_title)
    {
        $this->totalItems = array();
        $this->page_title = $page_title;
        // array_push($this->totalItems, array(
        //     "text" => "Home",
        //     "link" => "welcome.php")
        // );
    }

    public function addTabMenu($text, $link = "", $active = "")
    {
        array_push(
            $this->totalItems,
            array(
                "text" => $text,
                "link" => $link,
                "active" => $active
            )
        );
    }

    public function getTabMenu()
    {



        $result = '       <div class="page-tab_list_wrapper">
                                 <div class="site-wrapper">
                                 <div class="page-tab_trigger"></div>
                                 <div class="page-tab_curr">' . $this->page_title . '</div>
                                 <div class="page-tab_list">
        ';

        foreach ($this->totalItems as $TabMenu) {
            $text = $TabMenu['text'];
            $link = $TabMenu['link'];
            $active = $TabMenu['active'];

            if ($link != "#" && $link != "") {
                $result .= '  <a class="page-tab_list_item ' . $active . '"  href="' . $link . '"> ' . $text . '</a>';
            } else {
                $result .= '  <a class="page-tab_list_item ' . $active . '"  > ' . $text . '</a>';
            }
        }

        $result .= '     </div>
                      </div>
                 </div>
';


        return $result;
    }
    public function getTabMenuLength()
    {
        return count($this->totalItems);
    }
}
