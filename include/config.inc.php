<?php

DEFINE('IS_PROD', false);


$HOST = $_SERVER["SERVER_NAME"];
// $BASE_URL  = htmlspecialchars("http://" . $HOST . "/");
$CURRURL   = htmlspecialchars("http://" . $HOST . $_SERVER["REQUEST_URI"]);


$TITLE_EN     = "Race Relations Unit";
$TITLE_TC     = "種族關係組";
$TITLE_SC     = "种族关系组";



// $IMAGE_PATH = $_SERVER['DOCUMENT_ROOT'] . "/images/";
// $FILE_PATH = $_SERVER['DOCUMENT_ROOT'] . "/files/";
// $LIBRARY_PATH = $_SERVER['DOCUMENT_ROOT'] . "/files/";
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] . "/";


if (IS_PROD) {
    $root_path = $_SERVER['DOCUMENT_ROOT'] . "/";
    $BASE_URL  = htmlspecialchars("http://" . $HOST . "/");
} else {
    // $root_path =  "http://localhost/gov_hadla/";
    // $BASE_URL  = "http://localhost/gov_hadla/";

    $root_path =  "http://project24.creasant.in/gov_rru/";
    $BASE_URL  = "http://project24.creasant.in/gov_rru/";
}
// $root_path = $_SERVER['DOCUMENT_ROOT']."/gov_hadla/";
